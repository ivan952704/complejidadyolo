﻿
﻿using System;
using System.Drawing;

namespace TrabajoParcial
{
    public abstract class CPieza
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Color Color { get; set; }
        public String nombre { get; set; }
        public int numCasilla { get; set; }
        public int peso;
        

        public CPieza(int x, int y, Color color, int numCasilla)
        {
            this.X = x;
            this.Y = y;
            this.Color = color;
            this.numCasilla = numCasilla;
        }

        public void Pintar(Graphics g, int panelX, int panelY)
        {
            var Brushh = new SolidBrush(Color);
            g.FillEllipse(Brushh, X, Y, panelX/8, panelY/8);
            g.DrawString(nombre, new Font("Arial", 10), Brushes.DarkRed, new Point(X + panelX/18, Y + panelY/18));

        }
        

        public abstract Boolean Mover(CCasillero casillero, CTablero tabla);
        public abstract Boolean Comer(CCasillero casillero, CTablero tabla);
        //public abstract bool REcibe(CTablero tabla);
    }
}
