﻿
using System;
using System.Drawing;

namespace TrabajoParcial
{
    public class CPeon : CPieza
    {
        public CPeon(int x, int y, Color color, int numCasilla) : base(x, y, color, numCasilla)
        {
            nombre = "peon";
            peso = 1;
        }

        public override Boolean Comer(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null && Color != casillero.pieza.Color)
            {
                if (Color == Color.Black)
                {
                    if (numCasilla - casillero.NumCasilla == -11 || numCasilla - casillero.NumCasilla == -9)
                        return true;
                }
                else
                {
                    if (numCasilla - casillero.NumCasilla == 11 || numCasilla - casillero.NumCasilla == 9)
                        return true;
                }
            }
            return false;
        }

        public override bool Mover(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null)
                return false;

            if (Color == Color.Black)
            {
                if (numCasilla/10 == 1)
                {
                    if (casillero.NumCasilla - numCasilla == 10 || casillero.NumCasilla - numCasilla == 20)
                        return true;
                }

                if (casillero.NumCasilla - numCasilla == 10)
                {
                    return true;
                }
            }
            else
            {
                if (numCasilla/10 == 6)
                {
                    if (casillero.NumCasilla - numCasilla == -10 || casillero.NumCasilla - numCasilla == -20)
                        return true;
                }
                if (casillero.NumCasilla - numCasilla == -10)
                    return true;
            }

            return false;
        }
    }
}
