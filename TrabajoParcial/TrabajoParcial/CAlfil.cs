﻿
using System;
using System.Collections.Generic;
using System.Drawing;

namespace TrabajoParcial
{
    public class CAlfil : CPieza
    {
        
        public CAlfil(int x, int y, Color color, int numCasilla) : base(x, y, color, numCasilla)
        {
            nombre = "Alfil";
            peso = 4;
        }

        public override Boolean Comer(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null && Color != casillero.pieza.Color)
            {
                if (Math.Abs(casillero.NumCasilla - numCasilla)%11 == 0)
                    return true;
                if (Math.Abs(casillero.NumCasilla - numCasilla)%9 == 0)
                    return true;
            }
            return false;
        }

        public override bool Mover(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null)
                return false;

            List<int> diagonalizq = new List<int>();
            List<int> diagonalder = new List<int>();

            if (Math.Abs(casillero.NumCasilla - numCasilla)%11 == 0 ||
                Math.Abs(casillero.NumCasilla - numCasilla)%9 == 0)
                return true;

            return false;
        }
    }
}
