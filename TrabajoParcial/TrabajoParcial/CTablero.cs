﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace TrabajoParcial
{
    public class CTablero
    {
        public int contBlancas;
        public int contNegras;
        public int contReyBlanco;
        public int contReyNegro;
        public int SumaDePesosNegros;
        public int SumaDePesosBlancas;
        public List<List<CCasillero>> tablero { get; set; }
        public List<CCasillero> CasillerosNegros;
        public List<CCasillero> CasillerosPosibles;
        public List<CCasillero> CasillerosBlancos;
        public List<CCasillero> CasillerosPosibles_Blancos;
        public List<CCasillero> CasillerosPosibles_Negros;

        public CTablero()
        {
            tablero = new List<List<CCasillero>>();
            contBlancas = contNegras = 16;
            contReyBlanco = contReyNegro = 1;
            
            CasillerosPosibles = new List<CCasillero>();
            CasillerosNegros = new List<CCasillero>();
            CasillerosBlancos = new List<CCasillero>();
            CasillerosPosibles_Blancos = new List<CCasillero>();
            CasillerosPosibles_Negros = new List<CCasillero>();
        }

        public void PesosBlancas()
        {
            for(int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (tablero[i][j].pieza != null && tablero[i][j].pieza.Color == Color.White)
                    {
                        SumaDePesosBlancas += tablero[i][j].pieza.peso;
                    }
                }
            }
        }

        public void PesosNegras()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (tablero[i][j].pieza != null && tablero[i][j].pieza.Color == Color.Black)
                    {
                        SumaDePesosNegros += tablero[i][j].pieza.peso;
                    }
                }
            }
        }
        public void CrearTablero(int x, int y)
        {
            for (int i = 0; i < 8; ++i)
            {
                tablero.Add(new List<CCasillero>());
                for (int j = 0; j < 8; ++j)
                {
                    if (j%2 == 0)
                    {
                        tablero[i].Add(new CCasillero(i*(x/8), j*(y/8), x/8, y/8, null, Color.RosyBrown));
                        tablero[i][j].NumCasilla = (i + j*10);
                    }
                    else
                    {
                        tablero[i].Add(new CCasillero(i*(x/8), j*(y/8), x/8, y/8, null, Color.Pink));
                        tablero[i][j].NumCasilla = (i + j*10);
                    }
                }
            }

            tablero[0][0].pieza = new CTorre(tablero[0][0].x, tablero[0][0].y, Color.Black, (0 + 2*0));
            tablero[1][0].pieza = new CCaballo(tablero[1][0].x, tablero[1][0].y, Color.Black, (1 + 2*0));
            tablero[2][0].pieza = new CAlfil(tablero[2][0].x, tablero[2][0].y, Color.Black, (2 + 2*0));
            tablero[3][0].pieza = new CReyna(tablero[3][0].x, tablero[3][0].y, Color.Black, (3 + 2*0));
            tablero[4][0].pieza = new CRey(tablero[4][0].x, tablero[4][0].y, Color.Black, (4 + 2*0));
            tablero[5][0].pieza = new CAlfil(tablero[5][0].x, tablero[5][0].y, Color.Black, (5 + 2*0));
            tablero[6][0].pieza = new CCaballo(tablero[6][0].x, tablero[6][0].y, Color.Black, (6 + 2*0));
            tablero[7][0].pieza = new CTorre(tablero[7][0].x, tablero[7][0].y, Color.Black, (7 + 2*0));


            for (int i = 0; i < 8; ++i)
                tablero[i][1].pieza = new CPeon(tablero[i][1].x, tablero[i][1].y, Color.Black, (i + 1*10));

            for (int i = 0; i < 8; ++i)
                tablero[i][6].pieza = new CPeon(tablero[i][6].x, tablero[i][6].y, Color.White, (i + 6*10));

            tablero[0][7].pieza = new CTorre(tablero[0][7].x, tablero[0][7].y, Color.White, (0 + 7*10));
            tablero[1][7].pieza = new CCaballo(tablero[1][7].x, tablero[1][7].y, Color.White, (1 + 7*10));
            tablero[2][7].pieza = new CAlfil(tablero[2][7].x, tablero[2][7].y, Color.White, (2 + 7*10));
            tablero[3][7].pieza = new CReyna(tablero[3][7].x, tablero[3][7].y, Color.White, (3 + 7*10));
            tablero[4][7].pieza = new CRey(tablero[4][7].x, tablero[4][7].y, Color.White, (4 + 7*10));
            tablero[5][7].pieza = new CAlfil(tablero[5][7].x, tablero[5][7].y, Color.White, (5 + 7*10));
            tablero[6][7].pieza = new CCaballo(tablero[6][7].x, tablero[6][7].y, Color.White, (6 + 7*10));
            tablero[7][7].pieza = new CTorre(tablero[7][7].x, tablero[7][7].y, Color.White, (7 + 7*10));

            SetTodosGris();
            

            

        }

        public void ActualizarTabla(List<List<CCasillero>> tablita)
        {
            tablero = tablita;
        }

        public CPieza EliminaPieza(CPieza pieza)
        {
            if (pieza.Color == Color.Black)
                contNegras--;
            else
                contBlancas--;

            if (pieza.nombre == "Rey" && pieza.Color == Color.Black)
            {
                contReyNegro = 0;
            }
            else
                if (pieza.nombre == "Rey" && pieza.Color == Color.White)
                contReyBlanco = 0;

            return pieza = null;
        }

        public void DibujarTablero(Graphics g, int panelX, int panelY)
        {

            for (int i = 0; i < 8; ++i)
                for (int j = 0; j < 8; ++j)
                    tablero[i][j].mostrar(g, panelX, panelY);
        }

        public CCasillero BuscarCasilla(int mousex, int mousey, int XPanel, int Ypanel)
        {

            for (int i = 0; i < 8; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    Rectangle puntero = new Rectangle(mousex, mousey, 1, 1);
                    Rectangle casilla = new Rectangle(tablero[i].ElementAt(j).x, tablero[i].ElementAt(j).y, XPanel/8,
                        Ypanel/8);

                    bool choque = puntero.IntersectsWith(casilla);
                    CCasillero a = tablero[i][j];

                    if (choque)
                        return a;
                }
            }
            
            return null;
        }

        public bool CambiarColorCasilla(CCasillero A, int cantidadClicks)
        {
            for (int i = 0; i < 8; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    if (tablero[i][j].NumCasilla == A.NumCasilla && A.pieza != null && cantidadClicks == 0)
                    {
                        tablero[i][j].color = Color.Yellow;
                        return true;
                    }

                    if (tablero[i][j].NumCasilla == A.NumCasilla && A.pieza == null && cantidadClicks > 0)
                    {
                        tablero[i][j].color = Color.Yellow;
                        return true;
                    }
                }
            }
            return false;
        }

        public void SetTodosGris()
        {
            for (int i = 0; i < 8; ++i)
            {
                if (i%2 == 0)
                {
                    tablero[i][0].color = Color.Blue;
                    tablero[i][1].color = Color.LightBlue;
                    tablero[i][2].color = Color.Blue;
                    tablero[i][3].color = Color.LightBlue;
                    tablero[i][4].color = Color.Blue;
                    tablero[i][5].color = Color.LightBlue;
                    tablero[i][6].color = Color.Blue;
                    tablero[i][7].color = Color.LightBlue;
                }
                else
                {
                    tablero[i][0].color = Color.LightBlue;
                    tablero[i][1].color = Color.Blue;
                    tablero[i][2].color = Color.LightBlue;
                    tablero[i][3].color = Color.Blue;
                    tablero[i][4].color = Color.LightBlue;
                    tablero[i][5].color = Color.Blue;
                    tablero[i][6].color = Color.LightBlue;
                    tablero[i][7].color = Color.Blue;
                }
            }
        }

        public void IntercambiarPiezas(CCasillero PrimerClick, CCasillero SegundoClick)
        {
            for (int i = 0; i < 8; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    if (tablero[i].ElementAt(j).NumCasilla == SegundoClick.NumCasilla)
                    {
                        tablero[i].ElementAt(j).pieza = PrimerClick.pieza;
                        tablero[i].ElementAt(j).pieza.numCasilla = SegundoClick.NumCasilla;

                        tablero[i].ElementAt(j).pieza.X = tablero[i].ElementAt(j).x;
                        tablero[i].ElementAt(j).pieza.Y = tablero[i].ElementAt(j).y;
                    }
                }

            }

            for (int i = 0; i < 8; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    if (tablero[i].ElementAt(j).NumCasilla == PrimerClick.NumCasilla)
                        tablero[i].ElementAt(j).pieza = null;
                }
            }
        }

        public void IntercambiarPostDeath(CCasillero PrimerClick, CCasillero SegundoClick)
        {
            for (int i = 0; i < 8; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    if (tablero[i].ElementAt(j).NumCasilla == SegundoClick.NumCasilla)
                    {
                        tablero[i].ElementAt(j).pieza = EliminaPieza(tablero[i].ElementAt(j).pieza);
                        tablero[i].ElementAt(j).pieza = PrimerClick.pieza;
                        tablero[i].ElementAt(j).pieza.numCasilla = SegundoClick.NumCasilla;

                        tablero[i].ElementAt(j).pieza.X = tablero[i].ElementAt(j).x;
                        tablero[i].ElementAt(j).pieza.Y = tablero[i].ElementAt(j).y;
                    }
                }

            }

            for (int i = 0; i < 8; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    if (tablero[i].ElementAt(j).NumCasilla == PrimerClick.NumCasilla)
                        tablero[i].ElementAt(j).pieza = null;
                }
            }

        }

        public void ObtenerPiezasTablero()
        {
            CasillerosBlancos.Clear();
            CasillerosNegros.Clear();
            CasillerosPosibles_Blancos.Clear();
            CasillerosPosibles_Negros.Clear();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    var casillero = tablero[i][j];
                    if (casillero.pieza != null && casillero.pieza.Color == Color.Black)
                        CasillerosNegros.Add(casillero);
                    else
                        if (casillero.pieza != null && casillero.pieza.Color == Color.White)
                        CasillerosBlancos.Add(casillero);
                    else
                        if (casillero.pieza == null)
                        {
                        CasillerosPosibles.Add(casillero);
                        }
                }
            }

            

            CasillerosPosibles_Blancos.AddRange(CasillerosPosibles);
            CasillerosPosibles_Blancos.AddRange(CasillerosBlancos);
            CasillerosPosibles_Negros.AddRange(CasillerosPosibles);
            CasillerosPosibles_Negros.AddRange(CasillerosNegros);
        }



      



    }
}
