﻿
using System;
using System.Drawing;

namespace TrabajoParcial
{
    public class CRey : CPieza
    {
        public CRey(int x, int y, Color color, int numCasilla) : base(x, y, color, numCasilla)
        {
            nombre = "Rey";
            peso = 100;
        }

        public override Boolean Comer(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null && Color != casillero.pieza.Color)
            {
                if (Math.Abs(casillero.NumCasilla - numCasilla) == 10
                    || Math.Abs(casillero.NumCasilla - numCasilla) == 1
                    || Math.Abs(casillero.NumCasilla - numCasilla) == 11
                    || Math.Abs(casillero.NumCasilla - numCasilla) == 9)
                    return true;
            }

            return false;
        }

        public override bool Mover(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null)
                return false;

            if (Math.Abs(casillero.NumCasilla - numCasilla) == 10
                || Math.Abs(casillero.NumCasilla - numCasilla) == 1
                || Math.Abs(casillero.NumCasilla - numCasilla) == 11
                || Math.Abs(casillero.NumCasilla - numCasilla) == 9)
                return true;

            return false;
        }
    }
}
