﻿
using System.Drawing;

namespace TrabajoParcial
{
    public class CCasillero
    {
        public int x { get; set; }
        public int y { get; set; }
        public int ancho { get; set; }
        public int largo { get; set; }
        public CPieza pieza { get; set; }
        public Color color { get; set; }
        public int NumCasilla { get; set; }

        public CCasillero(int x, int y, int ancho, int largo, CPieza pieza, Color color)
        {
            this.x = x;
            this.y = y;
            this.ancho = ancho;
            this.largo = largo;
            this.color = color;
            this.pieza = pieza;
            if (pieza != null)
                pieza.numCasilla = NumCasilla;
        }

        public void mostrar(Graphics g, int panelX, int panelY)
        {
            Brush Brushh = new SolidBrush(color);

            g.FillRectangle(Brushh, x, y, ancho, largo);
            g.DrawRectangle(Pens.Black, x, y, ancho, largo);
            if (pieza != null)
                pieza.Pintar(g, panelX, panelY);
        }
    }
}
