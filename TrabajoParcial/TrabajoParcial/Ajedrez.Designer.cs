﻿namespace TrabajoParcial
{
    partial class Ajedrez
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ajedrez));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.contadorNegras = new System.Windows.Forms.Label();
            this.contadorBlancas = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Turno = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(85, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(626, 517);
            this.panel1.TabIndex = 0;
            this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(760, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 35);
            this.label1.TabIndex = 1;
            this.label1.Text = "Negras :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(760, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 35);
            this.label2.TabIndex = 2;
            this.label2.Text = "Blancas:";
            // 
            // contadorNegras
            // 
            this.contadorNegras.AutoSize = true;
            this.contadorNegras.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contadorNegras.Location = new System.Drawing.Point(876, 26);
            this.contadorNegras.Name = "contadorNegras";
            this.contadorNegras.Size = new System.Drawing.Size(83, 33);
            this.contadorNegras.TabIndex = 3;
            this.contadorNegras.Text = "label3";
            // 
            // contadorBlancas
            // 
            this.contadorBlancas.AutoSize = true;
            this.contadorBlancas.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contadorBlancas.Location = new System.Drawing.Point(876, 61);
            this.contadorBlancas.Name = "contadorBlancas";
            this.contadorBlancas.Size = new System.Drawing.Size(83, 33);
            this.contadorBlancas.TabIndex = 4;
            this.contadorBlancas.Text = "label3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(760, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 35);
            this.label3.TabIndex = 5;
            this.label3.Text = "Turno:";
            // 
            // Turno
            // 
            this.Turno.AutoSize = true;
            this.Turno.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Turno.Location = new System.Drawing.Point(876, 96);
            this.Turno.Name = "Turno";
            this.Turno.Size = new System.Drawing.Size(83, 33);
            this.Turno.TabIndex = 6;
            this.Turno.Text = "label4";
            // 
            // Ajedrez
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 674);
            this.Controls.Add(this.Turno);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.contadorBlancas);
            this.Controls.Add(this.contadorNegras);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Ajedrez";
            this.Text = "Complejo Ajedrez";
            this.Load += new System.EventHandler(this.Ajedrez_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label contadorNegras;
        private System.Windows.Forms.Label contadorBlancas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Turno;
    }
}

