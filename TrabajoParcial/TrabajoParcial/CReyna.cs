﻿
using System;
using System.Drawing;

namespace TrabajoParcial
{
    public class CReyna : CPieza
    {
        public CReyna(int x, int y, Color color, int numCasilla) : base(x, y, color, numCasilla)
        {
            nombre = "Reyna";
            peso = 10;
        }

        public override Boolean Comer(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null && Color != casillero.pieza.Color)
            {
                if (Math.Abs(casillero.NumCasilla - numCasilla)%10 == 0)
                    return true;
                if (Math.Abs(casillero.NumCasilla - numCasilla) >= 1 && Math.Abs(casillero.NumCasilla - numCasilla) <= 7)
                    return true;
                if (Math.Abs(casillero.NumCasilla - numCasilla)%11 == 0)
                    return true;
                if (Math.Abs(casillero.NumCasilla - numCasilla)%9 == 0)
                    return true;
            }

            return false;
        }

        public override bool Mover(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null)
                return false;
            if (Math.Abs(casillero.NumCasilla - numCasilla)%11 == 0)
                return true;
            if (Math.Abs(casillero.NumCasilla - numCasilla)%9 == 0)
                return true;
            if (Math.Abs(casillero.NumCasilla - numCasilla)%10 == 0)
                return true;
            if (Math.Abs(casillero.NumCasilla - numCasilla) >= 1 && Math.Abs(casillero.NumCasilla - numCasilla) <= 7)
                return true;

            return false;
        }
    }
}
