﻿
using System;
using System.Drawing;

namespace TrabajoParcial
{
    public class CCaballo : CPieza
    {
        public CCaballo(int x, int y, Color color, int numCasilla) : base(x, y, color, numCasilla)
        {
            nombre = "Caballo";
            peso = 3;
        }

        public override Boolean Comer(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null && Color != casillero.pieza.Color)
            {
                if (Math.Abs(casillero.NumCasilla - numCasilla) == 12)
                    return true;
                if (Math.Abs(casillero.NumCasilla - numCasilla) == 21)
                    return true;
                if (Math.Abs(casillero.NumCasilla - numCasilla) == 19)
                    return true;
                if (Math.Abs(casillero.NumCasilla - numCasilla) == 8)
                    return true;
            }

            return false;
        }

        public override bool Mover(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null)
                return false;
            if (Math.Abs(casillero.NumCasilla - numCasilla) == 12)
                return true;
            if (Math.Abs(casillero.NumCasilla - numCasilla) == 21)
                return true;
            if (Math.Abs(casillero.NumCasilla - numCasilla) == 19)
                return true;
            if (Math.Abs(casillero.NumCasilla - numCasilla) == 8)
                return true;
            
            return false;
        }
    }
}
