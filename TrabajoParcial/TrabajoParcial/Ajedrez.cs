﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace TrabajoParcial
{
    public partial class Ajedrez : Form
    {
        private CTablero tabla;
        private int contClick;
        private CCasillero PrimerClick;
        private CCasillero SegundoClick;
        private int ContMovimientos;
       
        private bool Mi_Turno;
        private Random aleatorio;
        private List<Tuple<CCasillero, CCasillero>> muertes;
        private List<int> listaExclusiones;
        public List<CTablero> grafo;

        public Ajedrez()
        {
            InitializeComponent();
            tabla = new CTablero();
            contClick = 0;
            tabla.CrearTablero(panel1.Width, panel1.Height);
            listaExclusiones = new List<int> {4, 5, 10};
            grafo = new List<CTablero>();
            tabla.PesosBlancas();
            tabla.PesosNegras();
            tabla.ObtenerPiezasTablero();
            Mi_Turno = true;
            aleatorio = new Random();
            ContMovimientos = 0;
            muertes = new List<Tuple<CCasillero, CCasillero>>();
        }

        private void Ajedrez_Load(object sender, EventArgs e)
        {
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            BufferedGraphics buffer;
            BufferedGraphicsContext context;
            context = BufferedGraphicsManager.Current;
            buffer = context.Allocate(panel1.CreateGraphics(), panel1.DisplayRectangle);
            buffer.Graphics.Clear(Color.White);
          
            contadorBlancas.Text = tabla.contBlancas.ToString();
            contadorNegras.Text = tabla.contNegras.ToString();
           
            if (tabla.contReyBlanco == 0 || tabla.contReyNegro == 0)
            {
                timer1.Enabled = false;
                MessageBox.Show("Fin del juego");
            }

            if (Mi_Turno)
                Turno.Text = "Querubin";
            else
            {
                Turno.Text = "PC OP";


             
               tabla = recursivo(grafo, tabla.SumaDePesosNegros, tabla.SumaDePesosBlancas, true,0);
              
                
               

                Mi_Turno = true;
                ContMovimientos++;
            }

            tabla.DibujarTablero(buffer.Graphics, panel1.Width, panel1.Height);

            buffer.Render();
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            if (Mi_Turno)
            {
                if (e.X > panel1.Width || e.Y > panel1.Height)
                    return;

                if (contClick >= 1)
                {
                    SegundoClick = tabla.BuscarCasilla(e.X, e.Y, panel1.Width, panel1.Height);

                    tabla.SetTodosGris();

                    bool intercambio = PrimerClick.pieza.Mover(SegundoClick, tabla);
                    bool intercambiomatar = PrimerClick.pieza.Comer(SegundoClick, tabla);
                    if (intercambio)
                    {
                        tabla.IntercambiarPiezas(PrimerClick, SegundoClick);
                        Mi_Turno = false;
                    }
                    if (intercambiomatar)
                    {
                        tabla.IntercambiarPostDeath(PrimerClick, SegundoClick);
                        Mi_Turno = false;
                    }

                    contClick = 0;
                }
                else
                {
                    PrimerClick = tabla.BuscarCasilla(e.X, e.Y, panel1.Width, panel1.Height);

                    if (PrimerClick.pieza != null && PrimerClick.pieza.Color == Color.White)
                        if (tabla.CambiarColorCasilla(PrimerClick, contClick))
                            contClick++;
                }
            }
        }

        public CTablero recursivo(List<CTablero> grafo, int pesonegros, int pesoblancos, bool turnoPC,int n)
        {
            CTablero NuevaTabla = new CTablero();
            NuevaTabla = tabla;

            tabla.ObtenerPiezasTablero();

           
            if (tabla.contBlancas < tabla.contNegras && n < 8 )
                return grafo.ElementAt(0);
            

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (tabla.tablero[n][j].pieza != null && tabla.tablero[n][j].pieza.Color == Color.Black && turnoPC == true)
                    {
                        for (int indice = 0; indice < tabla.CasillerosPosibles_Blancos.Count; indice++)
                        {
                            if (verificarMovimiento(tabla.tablero[n][j], tabla.CasillerosPosibles_Blancos.ElementAt(indice), tabla))
                            {
                                grafo.Add(tabla);
                                 return recursivo(grafo, tabla.SumaDePesosNegros, tabla.SumaDePesosBlancas, false,n);
                               
                                
                            }
                        }


                    }
                    
                   
                }
            }


            
                for (int j = 0; j < 8; j++)
                {
                    if (tabla.tablero[n][j].pieza != null && tabla.tablero[n][j].pieza.Color == Color.White && turnoPC == false)
                    {
                        for (int indice2 = 0; indice2 < tabla.CasillerosPosibles_Negros.Count; indice2++)
                        {
                            if (verificarMovimiento(tabla.tablero[n][j], tabla.CasillerosPosibles_Negros.ElementAt(indice2), tabla))
                            {
                                grafo.Add(tabla);
                                return recursivo(grafo, tabla.SumaDePesosNegros, tabla.SumaDePesosBlancas, true,n);
                               
                            }
                        }
                    }
                }

            tabla = NuevaTabla;
            return recursivo(grafo, tabla.SumaDePesosNegros, tabla.SumaDePesosBlancas, true, n+1);


        }

        //public List<CTablero> PosiblesJugadas(CTablero TableroActual,CCasillero Origen,CCasillero fin)
        //{
        //    List<CTablero> ListPosibleJugadas = new List<CTablero>();
        //    for (int i = 0; i < 8; i++)
        //    {
        //        for(int  j =0; j<8; j++)
        //        {
        //            if (TableroActual != null && TableroActual.tablero[i][j].pieza.Color == Color.White)
        //            {
        //                if (verificarMovimiento(Origen, fin, TableroActual))
        //                {

        //                    ListPosibleJugadas.Add(TableroActual);
                            
        //                }
                            
        //            }
        //        }
        //    }

        //    return ListPosibleJugadas;
        //}


        public bool verificarMovimiento(CCasillero origen, CCasillero fin, CTablero tablanueva)
        {


            if (origen.pieza != null)
            {
                if (origen.pieza.Mover(fin, tablanueva))
                {
                    tabla.IntercambiarPiezas(origen, fin);
                    return true;

                }

                //if (origen.pieza.Comer(fin, tablanueva))
                //{
                //    tabla.IntercambiarPostDeath(origen, fin);
                //    return true;
                //}
            }

            return false;


        }


    }
}
