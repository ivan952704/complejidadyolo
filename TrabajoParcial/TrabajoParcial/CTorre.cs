﻿
using System;
using System.Collections.Generic;
using System.Drawing;

namespace TrabajoParcial
{
    public class CTorre : CPieza
    {
        public CTorre(int x, int y, Color color, int numCasilla) : base(x, y, color, numCasilla)
        {
            nombre = "Torre";
            peso = 5;
        }

        public override Boolean Comer(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null && Color != casillero.pieza.Color)
            {
                if (Math.Abs(casillero.NumCasilla - numCasilla)%10 == 0)
                    return true;
                if (Math.Abs(casillero.NumCasilla - numCasilla) >= 1 && Math.Abs(casillero.NumCasilla - numCasilla) <= 7)
                    return true;
            }
            return false;
        }

        public override bool Mover(CCasillero casillero, CTablero tabla)
        {
            if (casillero.pieza != null)
                return false;

            List<int> movimientcolumna = new List<int>();
            List<int> movimientfila = new List<int>();

            int columna = numCasilla/10;
            int fila = numCasilla%10;

            for (int i = 0; i < 8; ++i)
            {
                if (i < fila)
                {
                    movimientfila.Add(tabla.tablero[i][columna].NumCasilla);
                    if (tabla.tablero[i][columna].pieza != null && tabla.tablero[i][columna].pieza.Color == Color)
                        movimientfila.Clear();
                }
                else if (i > fila)
                {
                    movimientfila.Add(tabla.tablero[i][columna].NumCasilla);
                    if (tabla.tablero[i][columna].pieza != null)
                        break;
                }
            }

            for (int i = 0; i < 8; ++i)
            {
                if (i < columna)
                {
                    movimientcolumna.Add(tabla.tablero[fila][i].NumCasilla);
                    if (tabla.tablero[fila][i].pieza != null && tabla.tablero[fila][i].pieza.Color == Color)
                        movimientcolumna.Clear();
                }
                else if (i > columna)
                {
                    movimientcolumna.Add(tabla.tablero[fila][i].NumCasilla);
                    if (tabla.tablero[fila][i].pieza != null)
                        break;
                }
            }

            movimientfila.AddRange(movimientcolumna);

            if (Math.Abs(casillero.NumCasilla - numCasilla)%10 == 0 ||
                Math.Abs(casillero.NumCasilla - numCasilla) >= 1 && Math.Abs(casillero.NumCasilla - numCasilla) <= 7)
            {
                for (int i = 0; i < movimientfila.Count; ++i)
                    if (casillero.NumCasilla == movimientfila[i])
                        return true;
            }

            return false;
        }
    }
}
